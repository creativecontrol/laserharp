#!/usr/bin/python

# Diavolo Laser Harp sensor input module
#
#
#
#
#

import VL53L0X


class SensorInput:

    def __init__(self, num_sensors):
        self.number_of_sensors = num_sensors
        self.TOF_SENSORS = []
        self.mode = VL53L0X.VL53L0X_LONG_RANGE_MODE  # this may be configurable at a later time
        self.sensor_values = []

        self.init_sensors()

    def init_sensors(self):
        # Create a sensor object for each sensor on the correct bus number.
        for sensor in range(self.number_of_sensors):
            self.TOF_SENSORS.append(VL53L0X.VL53L0X(TCA9548A_Num=sensor+1, TCA9548A_Addr=0x70))

    def start_sensors(self):
        for sensor in self.TOF_SENSORS:
            sensor.start_ranging(self.mode)

    def stop_sensors(self):
        for sensor in self.TOF_SENSORS:
            sensor.stop_ranging(self.mode)

    def read_sensor(self, sensor):
        return sensor.get_distance()
