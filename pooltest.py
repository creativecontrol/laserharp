import multiprocessing
import random
import time

last_num = 0


def rand_num():
    global last_num
    name = multiprocessing.current_process().name
    num = random.random()
    if num != last_num:
        print(name + ' ' + str(num))
        last_num = num
    else:
        print('wait')
    time.sleep(1)
    rand_num()


if __name__ == '__main__':
    thing1 = multiprocessing.Process(name='rand1', target=rand_num())
    thing2 = multiprocessing.Process(name='rand2', target=rand_num())
    thing1.start()
    thing2.start()