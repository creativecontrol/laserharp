#!/usr/bin/python

# Diavolo Laser Harp MQTT client module
#
#
#
#
#
import paho.mqtt.client as mqtt
import logging


class MQTTClient:

    def __init__(self, ip, port):
        self.server = mqtt.Client()
        self.server.on_connect = self.on_connect
        self.server.on_message = self.on_message
        self.server.connect(ip, port)

    def on_connect(self, master, userdata, flags, rc):
        print("connected with result code "+str(rc))
        self.send("sendback", "hello")

    def on_disconnect(self, client, userdata, rc=0):
        logging.debug("Disconnected result code " + str(rc))
        self.server.loop_stop()

    def on_message(self, master, userdata, msg):
        print(msg.topic + " " + str(msg.payload))
        # self.send("sendback", "hello2")

    def send(self, topic, payload):
        self.server.publish(topic, payload)

# if __name__ == '__main__':

