#!/usr/bin/python

# Diavolo Laser Harp main program
#
#
#
#
#

import configparser
import sensor_dummy as _sensors
# import sensor_input as _sensors
import mqtt_client
from decorators import *


class LaserHarp:

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('config.cfg')

        self.number_of_sensors = self.config.getint('Sensors', 'number_of_sensors')
        self.broker_ip = self.config.get('MQTT', 'IP')
        self.broker_port = self.config.getint('MQTT', 'PORT')
        self.topic = self.config.get('MQTT', 'publish_topic')
        self.platform_no = self.config.get('Platform', 'platform_no')

        self.sensors = _sensors.SensorInput(self.number_of_sensors)
        self.sensors.start_sensors()

        self.topic = self.topic + '/platform' + self.platform_no + '/sensor'

        self.client = mqtt_client.MQTTClient(self.broker_ip, self.broker_port)

        self.run_client()

    def run_client(self):
        self.client.server.loop_start()

    @rate_limit(50)  # every 20ms
    def read_sensor(self, sensor_number):
        return self.sensors.read_sensor(sensor_number)

    # Start a thread for each sensor. When the sensor returns a value send it to the mqtt_client.
    @threaded
    def check_sensor(self, sensor_number):
        while True:
            msg = mqtt_client.mqtt.MQTTMessage()
            msg.topic = self.topic + str(sensor_number)
            msg.payload = self.read_sensor(sensor_number)

            self.send_msg(msg)

    def send_msg(self, message):
        print (message.topic + ' ' + str(message.payload))
        self.client.send(message.topic, message.payload)


if __name__ == '__main__':
    laser_harp = LaserHarp()
    print('started')
    for sensor in range(0, int(laser_harp.number_of_sensors)):
        laser_harp.check_sensor(sensor+1)

    while True:
        pass
