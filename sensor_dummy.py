#!/usr/bin/python

# Diavolo Laser Harp sensor input module
#
#
#
#
#
import random


class SensorInput:

    def __init__(self, num_sensors):
        self.number_of_sensors = num_sensors
        self.TOF_SENSORS = []

        self.sensor_values = [8000, 215, 200, 300, 8000, 8000, 8000, 8000, 500, 8000, 8000]

        self.init_sensors()

    def init_sensors(self):
        # Create a sensor object for each sensor on the correct bus number.
        print('initializing sensors')
        for sensor in range(self.number_of_sensors):
            self.TOF_SENSORS.append(sensor)

    def start_sensors(self):
        print('started all sensors')

    def stop_sensors(self):
        print('stopped all sensors')

    def read_sensor(self, sensor):
        return random.choice(self.sensor_values)
