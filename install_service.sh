#!/bin/bash

sudo cp diavolo.service /lib/systemd/system
sudo chmod 664 /lib/systemd/system/diavolo.service
sudo systemctl daemon-reload
sudo systemctl enable diavolo.service